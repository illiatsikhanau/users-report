## Getting Started

1. Clone the repository.
2. Create a `.env.local` file in the root directory.
3. Add the necessary environment variables to the '.env.local' file:

NEXT_PUBLIC_API_URL=your_api_url

4. Save the `.env.local` file.
5. Run the development server using `npm run dev`.
6. View sql query and statistic in : `query.sql`, `query_statistic.png`.
