import '../../public/globals.css';
import {ReactNode} from 'react';
import type {Metadata} from 'next';
import Providers from '@/app/providers';

export const metadata: Metadata = {
    title: 'Users Report',
    description: 'Users Report',
}

const RootLayout = ({children}: { children: ReactNode }) => {
    return (
        <html lang='en'>
        <body className='flex content-center'>
        <Providers>
            {children}
        </Providers>
        </body>
        </html>
    );
}

export default RootLayout;
