'use client'

import {ChangeEvent, useEffect, useState} from 'react';
import User from '@/interfaces/User';
import Search from '@/components/Search';
import UserCard from '@/components/UserCard';

const Home = () => {
    const [search, setSearch] = useState('');
    const updateSearch = (e: ChangeEvent<HTMLInputElement>) => setSearch(e.target.value);

    const [users, setUsers] = useState([] as User[]);
    const fetchUsers = async () => {
        try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}?term=${search}`);
            const data = await response.json();
            setUsers(data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchUsers();
    }, []);

    return (
        <main className='w-full max-w-[1200px] flex flex-col gap-8 p-4 sm:px-20 sm:py-16'>
            <Search value={search} onChange={updateSearch} onSearch={fetchUsers}/>
            <div className='flex flex-wrap -m-3'>
                {users.map(user =>
                    <div key={user.email} className='w-full md:w-1/2 lg:w-1/3 flex p-3'>
                        <UserCard user={user}/>
                    </div>
                )}
            </div>
        </main>
    );
}

export default Home;
