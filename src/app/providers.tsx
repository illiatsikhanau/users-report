'use client'

import {ReactNode} from 'react';
import {NextUIProvider} from '@nextui-org/react';

const Providers = ({children}: { children: ReactNode }) => {
    return (
        <NextUIProvider className='w-full flex justify-center'>
            {children}
        </NextUIProvider>
    );
}

export default Providers;
