import {Modal, ModalContent, ModalHeader, ModalBody, useDisclosure} from '@nextui-org/react';
import User from '@/interfaces/User';
import PhoneIcon from '@/svg/PhoneIcon';
import MailIcon from '@/svg/MailIcon';
import CloseIcon from '@/svg/CloseIcon';

const UserCard = ({user}: { user: User }) => {
    const {isOpen, onOpen, onOpenChange} = useDisclosure();

    return (
        <>
            <div className='w-full h-[314px] flex flex-col gap-6 p-6 rounded-2xl
            shadow-card duration-300 hover:bg-lavender-gray cursor-pointer' onClick={onOpen}>
                <h2 className='text-2xl font-bold text-plumbum'>{user.name}</h2>
                <div className='flex flex-col gap-3'>
                    <div className='flex items-center gap-3.5 text-sm text-asphalt'>
                        <PhoneIcon className='w-6 h-6 fill-purple-fog'/>
                        <p className='w-[calc(100%-2.375rem)] break-all'>{user.phone}</p>
                    </div>
                    <div className='flex items-center gap-3.5 text-sm text-asphalt'>
                        <MailIcon className='w-6 h-6 fill-purple-fog'/>
                        <p className='w-[calc(100%-2.375rem)] break-all'>{user.email}</p>
                    </div>
                </div>
            </div>
            <Modal
                classNames={{
                    backdrop: 'bg-lavender-gray',
                    base: 'max-w-[500px] flex gap-10 p-6 shadow-card',
                    closeButton: 'w-7 h-7 mt-[22px] mr-4 opacity-0',
                }}
                isOpen={isOpen}
                onOpenChange={onOpenChange}
                placement='center'
            >
                <ModalContent>
                    <ModalHeader
                        className='flex items-center justify-between p-0 font-bold text-2xl text-plumbum'
                        as='h2'
                    >
                        {user.name}
                        <CloseIcon className='w-5 h-5 fill-black'/>
                    </ModalHeader>
                    <ModalBody className='flex gap-10 p-0'>
                        <div className='flex flex-col gap-3.5'>
                            <div className='flex justify-between'>
                                <p className='text-lg leading-6 text-plumbum'>Телефон:</p>
                                <p className='w-[calc(100%-11.5rem)] text-sm leading-6 break-all text-asphalt'>
                                    {user.phone}
                                </p>
                            </div>
                            <div className='flex justify-between'>
                                <p className='text-lg leading-6 text-plumbum'>Почта:</p>
                                <p className='w-[calc(100%-11.5rem)] text-sm leading-6 break-all text-asphalt'>
                                    {user.email}
                                </p>
                            </div>
                            <div className='flex justify-between'>
                                <p className='text-lg leading-6 text-plumbum'>Дата приема:</p>
                                <p className='w-[calc(100%-11.5rem)] text-sm leading-6 break-all text-asphalt'>
                                    {user.hire_date}
                                </p>
                            </div>
                            <div className='flex justify-between'>
                                <p className='text-lg leading-6 text-plumbum'>Должность:</p>
                                <p className='w-[calc(100%-11.5rem)] text-sm leading-6 break-all text-asphalt'>
                                    {user.position_name}
                                </p>
                            </div>
                            <div className='flex justify-between'>
                                <p className='text-lg leading-6 text-plumbum'>Подразделение:</p>
                                <p className='w-[calc(100%-11.5rem)] text-sm leading-6 break-all text-asphalt'>
                                    {user.department}
                                </p>
                            </div>
                        </div>
                        <div className='flex flex-col gap-3'>
                            <p className='text-lg text-plumbum'>Дополнительная информация:</p>
                            <p className='text-asphalt'>
                                Разработчики используют текст в качестве заполнителя макета страницы.
                                Разработчики используют текст в качестве заполнителя макета страницы.
                            </p>
                        </div>
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>
    );
}

export default UserCard;
