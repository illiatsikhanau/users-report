import {KeyboardEvent} from 'react';
import {Input, InputProps} from '@nextui-org/react';
import SearchIcon from '@/svg/SearchIcon';

interface Props extends InputProps {
    onSearch: () => void
}

const Search = (props: Props) => {
    const onKeyDown = (e: KeyboardEvent) => {
        if (e.key === 'Enter') {
            props.onSearch();
        }
    };

    return (
        <div className='w-full h-12 flex items-center gap-x-7 px-7 rounded-full border border-blueberry'>
            <Input {...{
                ...props,
                classNames: {
                    ...props.classNames,
                    base: 'w-[calc(100%-3rem)] bg-white text-plumbum',
                    inputWrapper: 'bg-white hover:bg-white data-[hover=true]:bg-white group-data-[focus=true]:bg-white',
                },
                onKeyDown: onKeyDown,
            }}/>
            <SearchIcon className='w-5 h-5 fill-purple-fog cursor-pointer' onClick={props.onSearch}/>
        </div>
    );
}

export default Search;
