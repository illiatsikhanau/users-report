SET STATISTICS TIME ON;

WITH subdivision_cte AS (
    SELECT 
		id, 
		name, 
		parent_id, 
		0 as nesting_level
    FROM subdivisions
    WHERE parent_id = (
		SELECT subdivision_id 
		FROM collaborators 
		WHERE id = 710253
	)
    UNION ALL
    SELECT s.id, s.name, s.parent_id, nesting_level + 1
    FROM subdivisions s
    JOIN subdivision_cte cte ON s.parent_id = cte.id
)

SELECT 
	c.id, 
	c.name, 
	cte.name AS sub_name,
	c.subdivision_id AS sub_id,
	cte.nesting_level AS sub_level,
	cc.colls_count
FROM collaborators c
JOIN subdivision_cte cte ON c.subdivision_id = cte.id
JOIN (
		SELECT cl.subdivision_id, COUNT(*) as colls_count
		FROM collaborators cl 
		GROUP BY cl.subdivision_id
	) cc ON cc.subdivision_id = c.subdivision_id
WHERE 
	c.age < 40
	AND LEN(c.name) > 11
	AND c.subdivision_id NOT IN (100055, 100059)
ORDER BY cte.nesting_level;

SET STATISTICS TIME OFF;
