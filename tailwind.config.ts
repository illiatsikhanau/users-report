import type {Config} from 'tailwindcss';
import {nextui} from '@nextui-org/react';

const config: Config = {
    content: [
        './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
        './src/components/**/*.{js,ts,jsx,tsx,mdx}',
        './src/app/**/*.{js,ts,jsx,tsx,mdx}',
        './node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}',
    ],
    theme: {
        extend: {
            colors: {
                'blueberry': '#D4DEFE',
                'plumbum': '#262C40',
                'asphalt': '#8189A3',
                'purple-fog': '#432EAB',
                'lavender-gray': '#BCC3D080',
            },
            boxShadow: {
                'card': '0px 0px 20px 0px #0000001A',
            }
        },
    },
    darkMode: 'class',
    plugins: [nextui()]
}

export default config;
